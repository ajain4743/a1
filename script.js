/// name, quantity, price of all 5 items in Cart
const itemName = document.querySelectorAll(".product");
const itemPrice = document.querySelectorAll(".price");
const itemQuantity = document.querySelectorAll(".qty");

// All 5 cart buttons for each item
const btnCart = document.querySelectorAll(".btnCart");
const btnCart1 = btnCart[0];
const btnCart2 = btnCart[1];
const btnCart3 = btnCart[2];
const btnCart4 = btnCart[3];
const btnCart5 = btnCart[4];

// check out button
const btnCheckOut = document.querySelector(".btnCheckOut");

// rName means Receipt Name >> r prefix used for receipt
// name, quantity and calculated price on reciept
const rName = document.querySelectorAll(".rProduct");
const rQuantity = document.querySelectorAll(".rQty");
const rPrice = document.querySelectorAll(".rPrice");

const rRows = document.querySelectorAll(".rRow"); // items on receipt
const customerName = document.querySelector(".name");
const gst = document.querySelector(".gst");
const totalPrice = document.querySelector(".totalPrice");
let total = 0;

// Function to be used when cart button is clicked >> to prompt quantity
enterQuantity = function (i) {
  let quantity = prompt(`How many ${itemName[i].innerHTML} would you like?`);
  while (isNaN(quantity) || quantity === "" || quantity === null) {
    quantity = prompt(`Please enter a number only`);
  }
  itemPrice[i] = parseInt(itemPrice[i].innerHTML.slice(1));
  itemQuantity[i].innerHTML = `x${quantity}`;
};

// Cart button Event invoked on all 5 items
btnCart1.addEventListener("click", function () {
  enterQuantity(0);
});

btnCart2.addEventListener("click", function () {
  enterQuantity(1);
});

btnCart3.addEventListener("click", function () {
  enterQuantity(2);
});

btnCart4.addEventListener("click", function () {
  enterQuantity(3);
});

btnCart5.addEventListener("click", function () {
  enterQuantity(4);
});

// Event on clicking Checkout
btnCheckOut.addEventListener("click", function () {
  const name = prompt(`Please enter your name`);
  customerName.innerHTML = `Customer name: ${name}`;

  for (let i = 0; i < 5; i++) {
    const quantity = parseFloat(itemQuantity[i].innerHTML.slice(1));
    // removes the items if quantity is blank
    if (quantity == 0 || isNaN(quantity) || quantity === "") {
      rRows[i].remove();
      continue;
    }

    rName[i].innerHTML = itemName[i].innerHTML;
    rQuantity[i].innerHTML = quantity;
    rPrice[i].innerHTML = `$${(
      parseFloat(itemPrice[i].innerHTML.slice(1)) * rQuantity[i].innerHTML
    ).toFixed(2)}`;
    total += parseFloat(rPrice[i].innerHTML.slice(1));
  }

  document.querySelector(".receiptTab").style.visibility = "visible";
  gst.innerHTML = `$${(total * 0.13).toFixed(2)}`;
  totalPrice.innerHTML = `$${(total * 1.13).toFixed(2)}`;

  if (total == 0) {
    document.querySelector(".receiptTab").style.visibility = "hidden";
    alert("You have not purchased anything.");
  }
});
